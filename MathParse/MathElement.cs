﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathParse
{
    public enum MathElementType
    {
        Default,
        Constant,
        Argument,
        Parameter,
        Function,
        BinOperator
    }

    public class MathElement
    {
        public MathElementType ElementType;
        public double Value;
        public bool IsNumeric
        {
            get {
                return (ElementType != MathElementType.BinOperator
                        && ElementType != MathElementType.Function);
            }
        }

        public MathElement()
        {
            ElementType = MathElementType.Default;
            Value = 0;
        }

        public MathElement(MathElementType elementType, double value)
        {
            ElementType = elementType;
            Value = value;
        }

        public MathElement(MathElement element)
        {
            ElementType = element.ElementType;
            Value = element.Value;
        }

        public override string ToString()
        {
            switch (ElementType)
            {
                case MathElementType.Argument:
                    if (Value == 1) return "x";
                    else if (Value == 2) return "y";
                    else if (Value == 3) return "z";
                    else return "?";
                case MathElementType.BinOperator:
                    var l1 = Constants.OperatorMap[this];
                    if (l1.Count > 0) return l1[0];
                    else return "?" + Value + "O?";
                case MathElementType.Constant:
                    var l2 = Constants.ConstantsMap[this];
                    if (l2.Count > 0) return l2[0];
                    else return Value.ToString();
                case MathElementType.Function:
                    var l3 = Constants.FunctionMap[this];
                    if (l3.Count > 0) return l3[0];
                    else return "?" + Value + "F?";
                case MathElementType.Parameter:
                    return "par" + (int)Value;
                    //return parameters[(int)Value][0] + " ";
                default:
                    return "<ERROR>";
            }
        }
    }

}
