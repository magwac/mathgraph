﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;

namespace MathParse
{
    public partial class Form1 : Form
    {
        MathParser parser = new MathParser();

        public Form1()
        {
            InitializeComponent();
            label1.Text = MathNet.Numerics.Control.LinearAlgebraProvider.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                parser.ReadInfix(richTextBox1.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            int i = 0;
            richTextBox2.Text = parser.WritePostfix();
            label3.Text = parser.CheckStack() ? "git" : "nope";
            MathEval mev = new MathEval(parser, new Dictionary<int, double>());
            parser.ElementStack = mev.CalcStack;
            richTextBox3.Text = parser.WritePostfix();

            List<Double3> args = new List<Double3>();
            /*args.Add(new Double3(1, 1, 0));
            args.Add(new Double3(1, 2, 2));
            args.Add(new Double3(-3, -1, -8));
            args.Add(new Double3(-4, 3, 1));
            DualPoly dp = new DualPoly(ref args);
            label1.Text = dp.Evaluate(2, 3).ToString();
            label8.Text = dp.WritePolyLong();
            label9.Text = dp.WritePolyShort();*/

        }

        private void button2_Click(object sender, EventArgs e)
        {
            MathEval mev = new MathEval(parser, new Dictionary<int, double>());
            numericUpDown3.Value = (decimal)mev.Evaluate((double)numericUpDown1.Value, (double)numericUpDown2.Value);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MathEval mev = new MathEval(parser, new Dictionary<int, double>());
            Double2 xr = new Double2(
                    (double)nudXa.Value,
                    (double)nudXb.Value);
            Double2 yr = new Double2(
                    (double)nudYa.Value,
                    (double)nudYb.Value);
            /*var t = mev.GetPoints(
                xr,
                yr,
                (int)nudPoints.Value
                );
            richTextBox4.Text = "";*/
            /*foreach (var me in t)
                richTextBox4.Text += me.ToString() + "\n";*/

            /*DualPoly dp = new DualPoly(ref t);
            for (double i = 0; i < 20; i++)
            {
                for (double j = 0; j < 20; j++)
                    richTextBox4.Text += dp.Evaluate(
                        (i / 20) * (xr.y-xr.x) + xr.x, 
                        (j / 20) * (yr.y-yr.x) + yr.x) + " ";
                richTextBox4.Text += Environment.NewLine;
            }*/
            List<double> ls = new List<double>();

            double pts = (double)nudPoints.Value;
            int n = (int)pts;
            for (double i = 0; i < pts; i++)
                for (double j = 0; j < pts; j++)
                    ls.Add(mev.Evaluate(
                        (i / pts) * (xr.y - xr.x) + xr.x,
                        (j / pts) * (yr.y - yr.x) + yr.x
                        ));
            /*richTextBox4.Text = "";

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                    richTextBox4.Text += ls[i * n + j];
                richTextBox4.Text += Environment.NewLine;
            }*/
            richTextBox4.Text = ls.Count + " " + ls[0];
                int a = 9;
            //richTextBox1.Text = dp.WritePolyLong();
        }

        private void evalHelper(object xyn)
        {
            /*points[(xyn as Tuple<double, double, int>).Item3]
                = mev.Evaluate(
                    (xyn as Tuple<double, double, int>).Item1,
                    (xyn as Tuple<double, double, int>).Item2);*/
        }
    }
}
