﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathParse
{
    public enum Fe
    {
        sin,
        cos,
        tg,
        ctg,
        sec,
        csc,
        asin,
        acos,
        atg,
        actg,
        asec,
        acsc,
        sinh,
        cosh,
        tgh,
        ctgh,
        exp,
        abs,
        ln,
        log10,
        ceil,
        floor,
        sqrt,
        sign,
        round
    }

    public enum Oe
    {
        add,
        subtract,
        multiply,
        divide,
        pow,
        mod,
        min,
        max
    }

    public static class Constants
    {
        public static BiDictionary<string, MathElement> FunctionMap;
        public static BiDictionary<string, MathElement> OperatorMap;
        public static BiDictionary<string, MathElement> ConstantsMap;


        #region Function elements
        public static readonly MathElement sin = new MathElement(
            MathElementType.Function,
            (int)Fe.sin
            );
        public static readonly MathElement cos = new MathElement(
            MathElementType.Function,
            (int)Fe.cos
            );
        public static readonly MathElement tg = new MathElement(
            MathElementType.Function,
            (int)Fe.tg
            );
        public static readonly MathElement ctg = new MathElement(
            MathElementType.Function,
            (int)Fe.ctg
            );
        public static readonly MathElement sec = new MathElement(
            MathElementType.Function,
            (int)Fe.sec
            );
        public static readonly MathElement csc = new MathElement(
            MathElementType.Function,
            (int)Fe.csc
            );
        public static readonly MathElement asin = new MathElement(
            MathElementType.Function,
            (int)Fe.asin
            );
        public static readonly MathElement acos = new MathElement(
            MathElementType.Function,
            (int)Fe.acos
            );
        public static readonly MathElement atg = new MathElement(
            MathElementType.Function,
            (int)Fe.atg
            );
        public static readonly MathElement actg = new MathElement(
            MathElementType.Function,
            (int)Fe.actg
            );
        public static readonly MathElement asec = new MathElement(
            MathElementType.Function,
            (int)Fe.asec
            );
        public static readonly MathElement acsc = new MathElement(
            MathElementType.Function,
            (int)Fe.acsc
            );
        public static readonly MathElement sinh = new MathElement(
            MathElementType.Function,
            (int)Fe.sinh
            );
        public static readonly MathElement cosh = new MathElement(
            MathElementType.Function,
            (int)Fe.cosh
            );
        public static readonly MathElement tgh = new MathElement(
            MathElementType.Function,
            (int)Fe.tgh
            );
        public static readonly MathElement ctgh = new MathElement(
            MathElementType.Function,
            (int)Fe.ctgh
            );
        public static readonly MathElement exp = new MathElement(
            MathElementType.Function,
            (int)Fe.exp
            );
        public static readonly MathElement abs = new MathElement(
            MathElementType.Function,
            (int)Fe.abs
            );
        public static readonly MathElement ln = new MathElement(
            MathElementType.Function,
            (int)Fe.ln
            );
        public static readonly MathElement log10 = new MathElement(
            MathElementType.Function,
            (int)Fe.log10
            );
        public static readonly MathElement ceil = new MathElement(
            MathElementType.Function,
            (int)Fe.ceil
            );
        public static readonly MathElement floor = new MathElement(
            MathElementType.Function,
            (int)Fe.floor
            );
        public static readonly MathElement sqrt = new MathElement(
            MathElementType.Function,
            (int)Fe.sqrt
            );
        public static readonly MathElement sign = new MathElement(
            MathElementType.Function,
            (int)Fe.sign
            );
        public static readonly MathElement round = new MathElement(
            MathElementType.Function,
            (int)Fe.round
            );
        #endregion

        #region Operator elements
        public static readonly MathElement add = new MathElement(
            MathElementType.BinOperator,
            (int)Oe.add
            );
        public static readonly MathElement subtract = new MathElement(
            MathElementType.BinOperator,
            (int)Oe.subtract
            );
        public static readonly MathElement multiply = new MathElement(
            MathElementType.BinOperator,
            (int)Oe.multiply
            );
        public static readonly MathElement divide = new MathElement(
            MathElementType.BinOperator,
            (int)Oe.divide
            );
        public static readonly MathElement pow = new MathElement(
            MathElementType.BinOperator,
            (int)Oe.pow
            );
        public static readonly MathElement mod = new MathElement(
            MathElementType.BinOperator,
            (int)Oe.mod
            );
        public static readonly MathElement min = new MathElement(
            MathElementType.BinOperator,
            (int)Oe.min
            );
        public static readonly MathElement max = new MathElement(
            MathElementType.BinOperator,
            (int)Oe.max
            );
        public static readonly MathElement lBracket = new MathElement(
            MathElementType.BinOperator,
            100
            );
        public static readonly MathElement rBracket = new MathElement(
            MathElementType.BinOperator,
            101
            );
        #endregion

        #region Constant elements
        public static readonly MathElement e = new MathElement(
            MathElementType.Constant,
            (double)Math.E
            );
        public static readonly MathElement pi = new MathElement(
            MathElementType.Constant,
            (double)Math.PI
            );
        public static readonly MathElement phi = new MathElement(
            MathElementType.Constant,
            (Math.Sqrt(5) + 1) / 2
            );
        #endregion

        /// <summary>
        /// Initialises dictionaries
        /// </summary>
        public static void Initialise()
        {
            FunctionMap = new BiDictionary<string, MathElement>();
            OperatorMap = new BiDictionary<string, MathElement>();
            ConstantsMap = new BiDictionary<string, MathElement>();

            addOtherFunc();
            addTrigonometric();
            addCiclometric();
            addHyperbolic();

            addOperators();

            ConstantsMap.Add("e", e);
            ConstantsMap.Add("pi", pi);
            ConstantsMap.Add("phi", phi);
            ConstantsMap.Add("fi", phi);
        }

        private static void addOperators()
        {
            OperatorMap.Add("+", add);
            OperatorMap.Add("plus", add);
            OperatorMap.Add("-", subtract);
            OperatorMap.Add("minus", subtract);
            OperatorMap.Add("*", multiply);
            OperatorMap.Add("mul", multiply);
            OperatorMap.Add("/", divide);
            OperatorMap.Add("\\", divide);
            OperatorMap.Add(":", divide);
            OperatorMap.Add("div", divide);
            OperatorMap.Add("^", pow);
            OperatorMap.Add("pow", pow);
            OperatorMap.Add("mod", mod);
            OperatorMap.Add("%", mod);
            OperatorMap.Add("modulo", mod);
            OperatorMap.Add("min", min);
            OperatorMap.Add("minimum", min);
            OperatorMap.Add("max", max);
            OperatorMap.Add("maximum", max);
        }

        private static void addOtherFunc()
        {
            FunctionMap.Add("exp", exp);
            FunctionMap.Add("e^", exp);
            FunctionMap.Add("abs", abs);
            FunctionMap.Add("ln", ln);
            FunctionMap.Add("log", log10);
            FunctionMap.Add("ceil", ceil);
            FunctionMap.Add("ceiling", ceil);
            FunctionMap.Add("floor", floor);
            FunctionMap.Add("sqrt", sqrt);
            FunctionMap.Add("sign", sign);
            FunctionMap.Add("sgn", sign);
            FunctionMap.Add("signum", sign);
            FunctionMap.Add("round", round);
        }

        private static void addTrigonometric()
        {
            FunctionMap.Add("sin", sin);
            FunctionMap.Add("sinus", sin);
            FunctionMap.Add("cos", cos);
            FunctionMap.Add("cosinus", cos);
            FunctionMap.Add("tg", tg);
            FunctionMap.Add("tan", tg);
            FunctionMap.Add("tangens", tg);
            FunctionMap.Add("ctg", ctg);
            FunctionMap.Add("cot", ctg);
            FunctionMap.Add("ctan", ctg);
            FunctionMap.Add("cotan", ctg);
            FunctionMap.Add("cotangens", ctg);
            FunctionMap.Add("sec", sec);
            FunctionMap.Add("secans", sec);
            FunctionMap.Add("csc", csc);
            FunctionMap.Add("cosecans", csc);
        }

        private static void addCiclometric()
        {
            FunctionMap.Add("asin", asin);
            FunctionMap.Add("arcsin", asin);
            FunctionMap.Add("arcussinus", asin);
            FunctionMap.Add("acos", acos);
            FunctionMap.Add("arccos", acos);
            FunctionMap.Add("arcuscosinus", acos);
            FunctionMap.Add("atg", atg);
            FunctionMap.Add("arctg", atg);
            FunctionMap.Add("atan", atg);
            FunctionMap.Add("arctan", atg);
            FunctionMap.Add("arcustangens", atg);
            FunctionMap.Add("actg", actg);
            FunctionMap.Add("acot", actg);
            FunctionMap.Add("arcctg", actg);
            FunctionMap.Add("actan", actg);
            FunctionMap.Add("arcctan", actg);
            FunctionMap.Add("acotan", actg);
            FunctionMap.Add("arccotan", actg);
            FunctionMap.Add("arcuscotangens", actg);
            FunctionMap.Add("asec", asec);
            FunctionMap.Add("arcsec", asec);
            FunctionMap.Add("arcussecans", asec);
            FunctionMap.Add("acsc", acsc);
            FunctionMap.Add("arccsc", acsc);
            FunctionMap.Add("arcuscosecans", acsc);
        }

        private static void addHyperbolic()
        {
            FunctionMap.Add("sinh", sinh);
            FunctionMap.Add("cosh", cosh);
            FunctionMap.Add("tgh", tgh);
            FunctionMap.Add("tanh", tgh);
            FunctionMap.Add("ctgh", ctgh);
            FunctionMap.Add("coth", ctgh);
            FunctionMap.Add("ctanh", ctgh);
            FunctionMap.Add("cotanh", ctgh);
        }
    }

}
