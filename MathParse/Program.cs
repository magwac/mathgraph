﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MathParse
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Constants.Initialise();
            MathNet.Numerics.Control.UseMultiThreading();
            //MathNet.Numerics.Control.UseNativeMKL();
            Application.Run(new Form1());
        }
    }
}
