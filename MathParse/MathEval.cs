﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics;
using System.Numerics;

namespace MathParse
{
    public class MathEval
    {
        public List<MathElement> CalcStack;

        public MathEval()
        {
            CalcStack = new List<MathElement>();
        }

        public MathEval(MathParser mp, Dictionary<int, double> paramValues)
        {
            CalcStack = new List<MathElement>();
            if (mp.CheckStack())
            {
                foreach (var me in mp.ElementStack)
                {
                    if (me.ElementType == MathElementType.Parameter)
                    {
                        double t = 0;
                        paramValues.TryGetValue((int)me.Value, out t);
                        CalcStack.Add(new MathElement(MathElementType.Constant, t));
                    }
                    else CalcStack.Add(me);
                }
                optimise(ref CalcStack);
            }
            else throw new Exception("Invalid stack");
        }

        /// <summary>
        /// Gets a list of points on the function's surface.
        /// </summary>
        /// <param name="xRange">The x range.</param>
        /// <param name="yRange">The y range.</param>
        /// <param name="points">Number of data points in each dimension.</param>
        /// <returns>List of points*points length</returns>
        public List<Double3> GetPoints(Double2 xRange, Double2 yRange, int points)
        {
            List<Double3> l = new List<Double3>();
            l.Capacity = points * points;       //faster
            Random r = new Random();

            double dx = (xRange.y - xRange.x) / points;
            double dy = (yRange.y - yRange.x) / points;
            for (int i = 0; i < points; i++)
                for (int j = 0; j < points; j++)
                {
                    double xx = xRange.x + dx * (i + r.NextDouble());
                    double yy = yRange.x + dy * (j + r.NextDouble());

                    l.Add(new Double3(
                        xx,
                        yy,
                        Evaluate(
                            xx,
                            yy
                            )));
                }

            return l;
        }

        /// <summary>
        /// Evaluates function value for specified x and y
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns>Function value</returns>
        public double Evaluate(double x, double y)
        {
            try
            {
                var st = new Stack<MathElement>();

                //można założyć że stos jest poprawny... (był sprawdzany w konstruktorze)
                //trochę to przyspiesza (duuużo mniej ifów i predykcja lepsza :3)

                foreach (var me in CalcStack)
                {
                    switch (me.ElementType)
                    {
                        case MathElementType.Argument:
                            if (me.Value == 1)
                                st.Push(new MathElement(MathElementType.Constant, x));
                            else if (me.Value == 2)
                                st.Push(new MathElement(MathElementType.Constant, y));
                            //else throw new Exception("Unknown argument type: '" + me.Value + "'.");
                            break;
                        case MathElementType.Constant:
                            st.Push(me);
                            break;
                        case MathElementType.Parameter:
                            throw new Exception("Unexpected parameter: '" + me.Value + "'.");
                        case MathElementType.BinOperator:
                            /*if (st.Count > 1)
                            {*/
                            var i1 = st.Pop();
                            var i2 = st.Pop();
                            /*if (i1.ElementType != MathElementType.Constant
                                || i2.ElementType != MathElementType.Constant)
                            {
                                throw new Exception("Not-evaluated operands.");
                            }
                            else*/
                            st.Push(new MathElement(MathElementType.Constant,
                                evalOperator(i2.Value, i1.Value, me.Value
                                ))); 
                            /*}
                            else throw new Exception("Too few operands for operator.");*/
                            break;
                        case MathElementType.Function:
                            /*if (st.Count > 0)
                            {*/
                                var i3 = st.Pop();
                                /*if (i3.ElementType != MathElementType.Constant)
                                {
                                    throw new Exception("Not-evaluated operands.");
                                }
                                else*/
                                st.Push(new MathElement(MathElementType.Constant,
                                    evalFunction(i3.Value, me.Value
                                    )));
                            //}
                            break;
                    }
                }
                if (st.Count == 1)
                {
                    var tm = st.Pop();
                    if (st.Count == 0 && tm.ElementType == MathElementType.Constant)
                        return tm.Value;
                    else throw new Exception("Not-evaluated expression on stack.");
                }
                else if (st.Count == 0) throw new Exception("Stack completely empty after evaluation.");
                else throw new Exception("Stack not empty after evaluation.");
            }
            catch (Exception ex)
            {
                throw new Exception("Evaluation error: " + ex.Message);
            }
        }

        /// <summary>
        /// Optimises passed stack by automagically calculating constant expressions
        /// </summary>
        private void optimise(ref List<MathElement> stack)
        {
            var st = new Stack<MathElement>();

            foreach (var me in stack)
            {
                switch (me.ElementType)
                {
                    case MathElementType.Argument:
                    case MathElementType.Constant:
                    case MathElementType.Parameter:
                        st.Push(me);
                        break;
                    case MathElementType.BinOperator:
                        if (st.Count > 1)
                        {
                            var i1 = st.Pop();
                            var i2 = st.Pop();
                            if (i1.ElementType != MathElementType.Constant
                                || i2.ElementType != MathElementType.Constant)
                            {
                                st.Push(i2);
                                st.Push(i1);
                                st.Push(me);
                            }
                            else st.Push(new MathElement(MathElementType.Constant,
                                evalOperator(i2.Value, i1.Value, me.Value
                                )));
                        }
                        break;
                    case MathElementType.Function:
                        if (st.Count > 0)
                        {
                            var i3 = st.Pop();
                            if (i3.ElementType != MathElementType.Constant)
                            {
                                st.Push(i3);
                                st.Push(me);
                            }
                            else st.Push(new MathElement(MathElementType.Constant, 
                                evalFunction(i3.Value, me.Value
                                )));
                        }
                        break;
                }
            }
            var st2 = new Stack<MathElement>(st);
            stack = new List<MathElement>();
            
            while(st2.Count > 0)
            {
                stack.Add(st2.Pop());
            }
        }

        /// <summary>
        /// Evaluates binary operator value
        /// </summary>
        /// <param name="x">l-value</param>
        /// <param name="y">r-value</param>
        /// <param name="type">Operator type</param>
        /// <returns></returns>
        private double evalOperator(double x, double y, double type)
        {
            Oe oe = (Oe)(int)type;

            switch (oe)
            {
                case Oe.add:
                    return x + y;
                case Oe.divide:
                    return x / y;
                case Oe.max:
                    return Math.Max(x, y);
                case Oe.min:
                    return Math.Min(x, y);
                case Oe.mod:
                    return x % y;
                case Oe.multiply:
                    return x * y;
                case Oe.pow:
                    return Math.Pow(x, y);
                case Oe.subtract:
                    return x - y;            
            }
            return x;
        }

        /// <summary>
        /// Evaluates function value
        /// </summary>
        /// <param name="x">argument</param>
        /// <param name="type">Function type</param>
        /// <returns></returns>
        private double evalFunction(double x, double type)
        {
            Fe fe = (Fe)(int)type;

            switch (fe)
            {
                case Fe.abs:
                    return Math.Abs(x);
                case Fe.acos:
                    return Trig.Acos(x);
                case Fe.acsc:
                    return Trig.Acsc(x);
                case Fe.actg:
                    return Trig.Acot(x);
                case Fe.asec:
                    return Trig.Asec(x);
                case Fe.asin:
                    return Trig.Asin(x);
                case Fe.atg:
                    return Trig.Atan(x);
                case Fe.ceil:
                    return Math.Ceiling(x);
                case Fe.cos:
                    return Trig.Cos(x);
                case Fe.cosh:
                    return Trig.Cosh(x);
                case Fe.csc:
                    return Trig.Csc(x);
                case Fe.ctg:
                    return Trig.Cot(x);
                case Fe.ctgh:
                    return Trig.Coth(x);
                case Fe.exp:
                    return Math.Exp(x);
                case Fe.floor:
                    return Math.Floor(x);
                case Fe.ln:
                    return Math.Log(x);
                case Fe.log10:
                    return Math.Log10(x);
                case Fe.sec:
                    return Trig.Sec(x);
                case Fe.sign:
                    return Math.Sign(x);
                case Fe.sin:
                    return Trig.Sin(x);
                case Fe.sinh:
                    return Trig.Sinh(x);
                case Fe.sqrt:
                    return Math.Sqrt(x);
                case Fe.tg:
                    return Trig.Tan(x);
                case Fe.tgh:
                    return Trig.Tanh(x);
                case Fe.round:
                    return Math.Round(x);
            }
            return x;
        }
    }
}
