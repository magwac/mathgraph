﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace MathParse
{
    /// <summary>
    /// Class for operations on polynomials with two variables: x and y.
    /// </summary>
    public class DualPoly
    {
        /// <summary>
        /// Gets or sets the max degree of a single variable. Setting it resizes neccessary lists.
        /// </summary>
        /// <value>
        /// maximum degree of the polynomial
        /// </value>
        public int MaxDegree
        {
            get
            {
                return maxDegree;
            }
            set
            {
                Coefs.Resize(value);
                foreach (var l in Coefs) l.Resize(value, 0F);
                maxDegree = value;
            }
        }

        /// <summary>
        /// Coefficients, arranged in a 2D list.
        /// Coef[x degree][y degree];
        /// </summary>
        public List<List<double>> Coefs = new List<List<double>>();

        private int maxDegree = 0;

        public DualPoly()
        {
            MaxDegree = 0;
        }

        public DualPoly(int degree)
        {
            MaxDegree = degree;
        }

        /// <summary>
        /// Initializes a new instance based on a set of points.
        /// The set must contain n^2 elements.
        /// Eats a ton of memory.
        /// </summary>
        /// <param name="points">Points to interpolate on.</param>
        /*public DualPoly(ref List<Double3> points)
        {
            int n = (int)Math.Sqrt(points.Count);
            int n2 = n * n;
            if (n2 == points.Count && n > 0)
            {
                double[][] m = new double[n2][];
                for (int i = 0; i < n2; i++) m[i] = new double[n2 + 1];

                //wpisujemy punkty
                for (int i = 0; i < n2; i++)
                {
                    m[i][n2] = points[i].z;     //wyraz wolny
                    double ym = 1;
                    for (int j = 0; j < n; j++)     //optimised for mem access
                    {
                        double xm = 1;
                        for (int k = 0; k < n; k++)
                        {
                            m[i][j * n + k] = xm * ym;
                            xm *= points[i].x;
                        }
                        ym *= points[i].y;
                    }
                }

                //Gauss
                for (int i = 0; i < n2; i++)
                {
                    double d = m[i][i];
                    if (d == 0)     //trza znaleźć zamiennik (no chyba że kolumna się wyzerowała)
                    {
                        for (int j=i+1; j<n2; j++)
                            if (m[j][i] != 0)
                                for (int k=0; k<n2+1; k++)
                                {
                                    double s = m[i][k];
                                    m[i][k] = m[j][k];
                                    m[j][k] = s;
                                }
                        d = m[i][i];
                    }
                    for (int j = 0; j < n2 + 1; j++) m[i][j] /= d;

                    for (int j = 0; j < n2; j++)
                        if (j != i)
                        {
                            double mul = m[j][i];
                            for (int k = 0; k < n2 + 1; k++)
                                m[j][k] -= (m[i][k] * mul);
                        }
                }

                //...i wpisujemy coefy
                MaxDegree = n;

                for (int i = 0; i < n; i++)
                    for (int j = 0; j < n; j++)
                    {
                        Coefs[i][j] = m[i * n + j][n2];
                    }
            }
            else MaxDegree = 0;
        }
        */

        /// <summary>
        /// Initializes a new instance based on a set of points.
        /// The set must contain n^2 elements.
        /// Eats a ton of memory.
        /// </summary>
        /// <param name="points">Points to interpolate on.</param>
        public DualPoly(ref List<Double3> points)
        {
            int n = (int)Math.Sqrt(points.Count);
            int n2 = n * n;
            if (n2 == points.Count && n > 0)
            {
                {
                    var m = Matrix<double>.Build.Dense(n2, n2);
                    var b = Vector<double>.Build.Dense(n2);

                    for (int i = 0; i < n2; i++)
                        b[i] = points[i].z;

                    //wpisujemy punkty
                    for (int i = 0; i < n2; i++)
                    {
                        double ym = 1;
                        for (int j = 0; j < n; j++)     //optimised for mem access
                        {
                            double xm = 1;
                            for (int k = 0; k < n; k++)
                            {
                                m[i, j * n + k] = xm * ym;
                                xm *= points[i].x;
                            }
                            ym *= points[i].y;
                        }
                    }

                    //Gauss :3
                    var s = m.Solve(b);

                    //...i wpisujemy coefy
                    MaxDegree = n;

                    for (int i = 0; i < n; i++)
                        for (int j = 0; j < n; j++)
                        {
                            Coefs[i][j] = s[i * n + j];
                        }
                }
                GC.Collect();
            }
            else MaxDegree = 0;
        }

        /// <summary>
        /// Evaluates the polynomial for specified x and y. 
        /// </summary>
        /// <param name="x">x</param>
        /// <param name="y">y</param>
        /// <returns>Polynomial value.</returns>
        public double Evaluate(double x, double y)
        {
            double s = 0;
            double xm = 1;
            foreach(var l in Coefs)
            {
                double ss = 0;
                foreach(var c in l)
                {
                    ss = ss * y + c;
                }
                s += ss * xm;
                xm *= x;
            }
            return s;
        }

        /// <summary>
        /// Writes the poly in a long sum format (compatible with infix input)
        /// </summary>
        /// <returns></returns>
        public string WritePolyLong()
        {
            string s = "";

            for (int i=0; i<maxDegree; i++)
                for (int j = 0; j<maxDegree; j++)
                {
                    s += Coefs[i][j].ToString("0.0000");
                    s += i == 0 ? "" : i == 1 ? "*x" : ("*x^" + i);
                    s += j == 0 ? "" : j == 1 ? "*y" : ("*y^" + j);
                    s += j < maxDegree - 1 || i < maxDegree - 1 ? " + " : "";
                }
            
            return s;
        }

        /// <summary>
        /// Writes the poly in a short sum format
        /// </summary>
        /// <returns></returns>
        public string WritePolyShort()
        {
            string s = "";

            for (int i = 0; i < maxDegree; i++)
                for (int j = 0; j < maxDegree; j++)
                {
                    s += Coefs[i][j].ToString("0.0000");
                    s += i == 0 ? "" : i == 1 ? "x" : ("x" + i);
                    s += j == 0 ? "" : j == 1 ? "y" : ("y" + j);
                    s += j < maxDegree - 1 || i < maxDegree - 1 ? " + " : "";
                }

            return s;
        }

        public override string ToString()
        {
            if (Coefs.Count < 5)
            {
                string s = "";
                foreach (var l in Coefs)
                {
                    foreach (var f in l)
                        s += f.ToString("0.00") + "\t";
                    s += Environment.NewLine;
                }
            }
            return base.ToString();
        }
    }
}
